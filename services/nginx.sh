#!/bin/bash

x=`ls /overlay/ | wc -l`

if [ $x -gt 0 ] ; then 
    cp -a /overlay/* / ;
fi

exec /usr/sbin/nginx -g 'daemon off;'
